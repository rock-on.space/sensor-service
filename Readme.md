This is the API for the sensor service. It will be responsible for storing and providing things like humidity, temperature, sound pressure level, recent tracks played.

It will not use authentication (for now) - This is called a "squishy centre" which is not advised for production, but at this stage of development it improves developer experience.

Draft API doc: https://app.swaggerhub.com/apis/Rock-On.Space/Sensor-Service/1.0.0
