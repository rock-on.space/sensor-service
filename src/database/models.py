from datetime import datetime, date
from uuid import UUID

from pony.orm import Database
from pony.orm import Optional as PonyOptional
from pony.orm import Required as PonyRequired
from pony.orm import Set as PonySet
from pony.orm import PrimaryKey as PonyPrimaryKey

from v1.config import settings

db = Database()


class SensorDB(db.Entity):  # type: ignore
    sensor_uid = PonyPrimaryKey(UUID, auto=True)
    music_detections = PonySet("MusicDetectionDB")  # type: ignore
    sensor_measurements = PonySet("SensorMeasurementDB")  # type: ignore


class MusicDetectionDB(db.Entity):  # type: ignore
    detection_id = PonyPrimaryKey(int, auto=True)
    sensor = PonyRequired(SensorDB)
    track = PonyRequired("TrackDB")  # type: ignore
    timestamp = PonyRequired(datetime, default=datetime.utcnow)


class TrackDB(db.Entity):  # type: ignore
    track_id = PonyPrimaryKey(int, auto=True)
    name = PonyRequired(str)
    music_detections = PonySet(MusicDetectionDB)
    album_image = PonyRequired(str)
    release_date = PonyOptional(date)
    artists = PonySet("ArtistDB")  # type: ignore
    genres = PonySet("GenreDB")  # type: ignore


class SensorMeasurementDB(db.Entity):  # type: ignore
    measurement_id = PonyPrimaryKey(int, auto=True)
    sensor = PonyRequired(SensorDB)
    sound_level = PonyOptional(float)
    music_level = PonyOptional(float)
    temperature = PonyRequired(float)
    humidity = PonyRequired(float)
    voc = PonyOptional(float)
    co2 = PonyOptional(float)
    timestamp = PonyRequired(datetime, default=datetime.utcnow)


class GenreDB(db.Entity):  # type: ignore
    genre_id = PonyPrimaryKey(int, auto=True)
    name = PonyRequired(str)
    tracks = PonySet(TrackDB)


class ArtistDB(db.Entity):  # type: ignore
    artist_id = PonyPrimaryKey(int, auto=True)
    name = PonyRequired(str)
    tracks = PonySet(TrackDB)


match settings.db_provider:
    case "sqlite":
        db.bind(
            provider=settings.db_provider, filename=settings.db_name, create_db=True
        )
    case "postgres":
        db.bind(
            provider=settings.db_provider,
            host=settings.db_host,
            port=settings.db_port,
            user=settings.db_username,
            password=settings.db_password,
            database=settings.db_name,
            sslmode=settings.db_sslmode,
        )
    case "cockroach":
        db.bind(
            provider=settings.db_provider,
            host=settings.db_host,
            port=settings.db_port,
            user=settings.db_username,
            password=settings.db_password,
            database=settings.db_name,
            sslmode=settings.db_sslmode,
        )
    case _:
        raise ValueError("Unknown database provider")


db.generate_mapping(create_tables=settings.db_create_tables)
