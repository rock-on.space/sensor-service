from __future__ import annotations

import logging

from fastapi import FastAPI


import v1.router
from database.models import SensorDB
from v1.models import Health

app = FastAPI(
    description="""
    This is the API for the sensor service.
    It will be responsible for storing and providing things like humidity, temperature, sound pressure level, recent tracks played.
    """,
    version="1.0.0",
    title="Sensor Service",
    contact={"email": "tim@rock-on.space"},
)


app.include_router(v1.router.router)


@app.get("/alive")
async def alive():
    return {"message": "Hello World"}


@app.get("/healthz")
async def health():
    response = Health(errors=[])
    try:
        SensorDB.select().page(0, 1)
    except Exception as e:
        logging.exception("failure to connect to database %s", type(e))
        response.errors.append("Error connecting to DB")
    return response
