from __future__ import annotations

from datetime import datetime, date
from typing import Optional, List
from uuid import UUID

from pydantic import BaseModel, Field, NoneStr

from v1.config import settings


class Health(BaseModel):
    service_name: str = settings.app_name
    version_number: str = settings.version_number
    errors: List[str]


class SensorEnvironInput(BaseModel):
    sound_level: Optional[float] = None
    music_level: Optional[float] = None
    temperature: float
    humidity: float
    voc: Optional[float] = None
    co2: Optional[float] = None

    class Config:
        orm_mode = True


class SensorEnvironOutput(SensorEnvironInput):
    measurement_id: int
    timestamp: datetime


class Genre(BaseModel):
    genre_uid: Optional[UUID]
    name: str


class MusicDetectionInput(BaseModel):
    name: str
    album_image: str
    release_date: date
    artists: List[str]
    genres: List[Genre]


class Track(MusicDetectionInput):
    track_id: int

    class Config:
        orm_mode = True


class MusicDetectionOutput(Track):
    detection_id: int
    sensor_uid: UUID
    timestamp: datetime


class MusicArray(BaseModel):
    next: NoneStr = Field(None, alias="_next")
    count: int = Field(None, alias="_count")
    data: List[MusicDetectionOutput]


class SensorComplete(SensorEnvironOutput):
    sensor_uid: Optional[UUID]
    current_track: MusicDetectionOutput
    previous_tracks: MusicArray
