from typing import List, Tuple
from uuid import UUID

from fastapi import APIRouter, HTTPException
from pony.orm import db_session, select, desc
import logging

from .models import (
    SensorComplete,
    Track,
    MusicDetectionOutput,
    SensorEnvironOutput,
    MusicArray,
    MusicDetectionInput,
    SensorEnvironInput,
    Genre,
)
from database.models import (
    SensorDB,
    SensorMeasurementDB,
    MusicDetectionDB,
    TrackDB,
    ArtistDB,
    GenreDB,
)

router = APIRouter(
    prefix="/v1",
    tags=["v1"],
)


async def get_music_by_sensor(
    sensor: SensorDB, page: int, page_size: int
) -> Tuple[int, List[MusicDetectionOutput]]:
    music_detection_query = select(
        detection
        for detection in MusicDetectionDB  # type: ignore
        if detection.sensor == sensor
    ).order_by(desc(MusicDetectionDB.timestamp))

    raw_music_detections = music_detection_query.page(page, page_size)
    music_detections = [
        MusicDetectionOutput(
            detection_id=detection.detection_id,
            sensor_uid=detection.sensor.sensor_uid,
            timestamp=detection.timestamp,
            **Track(
                **detection.track.to_dict(exclude=["artists", "genres"]),
                artists=[artist.name for artist in detection.track.artists],
                genres=[Genre.from_orm(genre) for genre in detection.track.genres],
            ).dict(),
        )
        for detection in raw_music_detections
    ]
    return music_detection_query.count(), music_detections


@router.get(
    "/sensor/{sensor_uid}",
    response_model=SensorComplete,
    response_model_exclude_none=True,
)
async def get_sensor(sensor_uid: UUID):
    """Get sensor by UID"""
    with db_session:
        if (raw_sensor := SensorDB.get(sensor_uid=sensor_uid)) is None:
            raise HTTPException(status_code=404, detail="Sensor not found")
        raw_latest_sensor_data = (
            SensorMeasurementDB.select()
            .order_by(desc(SensorMeasurementDB.timestamp))
            .first()
        )
        if raw_latest_sensor_data is None:
            raise HTTPException(status_code=204, detail="No Data yet")
        sensor_environ = SensorEnvironOutput.from_orm(raw_latest_sensor_data)

        num_music_detections, music_detections = await get_music_by_sensor(
            raw_sensor, 0, 10
        )

        next_page = (
            f"{router.prefix}/sensor/{raw_sensor.sensor_uid}/previous_tracks?page=1&page_size=10"
            if (num_music_detections - 11) > 0
            else None
        )
    return SensorComplete(
        sensor_uid=raw_sensor.sensor_uid,
        current_track=music_detections[0],
        previous_tracks=MusicArray(
            next=next_page, count=num_music_detections, data=music_detections  # type: ignore[call-arg]
        ),
        **sensor_environ.dict(),
    )


@router.get(
    "/sensor/{sensor_uid}/previous_tracks",
    response_model=MusicArray,
    response_model_exclude_none=True,
)
async def get_tracks(sensor_uid: UUID, page: int, page_size: int):
    with db_session:
        if (sensor := SensorDB.get(sensor_uid=sensor_uid)) is None:
            raise HTTPException(status_code=404, detail="Sensor not found")
        num_music_detections, music_detections = await get_music_by_sensor(
            sensor, page, page_size
        )

        next_page = (
            f"{router.prefix}/sensor/{sensor.sensor_uid}/previous_tracks?page={page+1}&page_size={page_size}"
            if (num_music_detections - page_size * (page + 1)) > 0
            else None
        )

        return MusicArray(
            next=next_page, count=num_music_detections, data=music_detections  # type: ignore[call-arg]
        )


@router.post(
    "/sensor/{sensor_uid}/music_detection",
    response_model=None,
    response_model_exclude_none=True,
)
async def post_music_detection(sensor_uid: UUID, music_detection: MusicDetectionInput):
    with db_session:
        if (raw_sensor := SensorDB.get(sensor_uid=sensor_uid)) is None:
            raise HTTPException(status_code=404, detail="Sensor not found")
        if (
            raw_artist_query := ArtistDB.select(
                lambda artist: artist.name in music_detection.artists
            )
        ).count() > len(music_detection.artists):
            for artist_name in music_detection.artists:
                logging.warning("artist_missing: %s", artist_name)
                ArtistDB(name=artist_name)
            raw_artist_query = ArtistDB.select(
                lambda artist: artist.name in music_detection.artists
            )

        raw_track_query = TrackDB.select(
            lambda track: track.name == music_detection.name
            and track.artists == raw_artist_query
        )
        if raw_track_query.count() > 0:
            raw_track = raw_track_query.first()
        else:
            raw_genres_query = GenreDB.select(
                lambda genre: genre.name in music_detection.genres
            )
            if raw_genres_query.count() < music_detection.genres:
                detected_genres = {genre.name for genre in music_detection.genres}
                found_genres = {raw_genre.name for raw_genre in raw_genres_query}
                missing_genres = detected_genres - found_genres
                logging.warning("genres_missing: %s", missing_genres)
            raw_track = TrackDB(
                name=music_detection.name,
                album_image=music_detection.album_image,
                release_date=music_detection.release_date,
                artists=raw_artist_query,
                genres=raw_genres_query,
            )
        MusicDetectionDB(sensor=raw_sensor, track=raw_track)


@router.post(
    "/sensor/{sensor_uid}/environment_measurement",
    response_model=None,
    response_model_exclude_none=True,
    status_code=201,
)
async def post_environment_measurement(
    sensor_uid: UUID, environment_measurement: SensorEnvironInput
):
    with db_session:
        if (raw_sensor := SensorDB.get(sensor_uid=sensor_uid)) is None:
            raise HTTPException(status_code=404, detail="Sensor not found")
        SensorMeasurementDB(
            sensor=raw_sensor,
            sound_level=environment_measurement.sound_level,
            music_level=environment_measurement.music_level,
            temperature=environment_measurement.temperature,
            humidity=environment_measurement.humidity,
            voc=environment_measurement.voc,
            co2=environment_measurement.co2,
        )
